from enum import Enum
# Enum assigns names to numbers.
# Makes it easier to keep track of what board state signifies
class Tile(Enum):
    EMPTY = 1
    WHITE = 2
    BLACK = 3