from game import Game
import time
from tile import Tile
from players.random_player import RandomPlayer
from players.human_player import HumanPlayer
from players.greedy_player import GreedyPlayer
from players.strategic_greedy_player import StrategicGreedyPlayer
from players.random_greedy_player import RandomGreedyPlayer
from players.random_strategic_greedy_player import RandomStrategicGreedyPlayer
from players.center_strategy_player import CenterStrategyPlayer
from players.defensive_player import DefensivePlayer
from players.balanced_defensive_player import BalancedDefensivePlayer

"""
This module is responsible for actually running the games
"""

"""
This function plays a single game with the given players and has options to configure what's displayed
"""
def play_game(player1, player2, time_game = False, print_board = False, print_turns = False, step_through_game = False, print_winner=False):
    start = None
    if time_game: 
        start = time.perf_counter()      
    
    # Create game
    game = Game(player1, player2)
    turn = 0

    while not game.is_over():
        if print_turns:
            print()
            print(f"Turn #{turn}: ")
        if print_board:
            game.print_board()
        turn+=1
        game.take_turn()
        
        if step_through_game: 
            input("Press enter to continue")
    if print_winner: 
        if(game.get_winner() == Tile.BLACK):
            print("Black Wins")
        elif(game.get_winner() == Tile.WHITE):
            print("White Wins")
    if time_game:
        print(f"{int((time.perf_counter()-start)*1000)}")
    return game
    
"""
This function plays n games and keeps track of the number of wins for each player. 
"""
def play_games(n, player1, player2):
    black_victories = 0
    white_victories = 0
    for i in range(n):  
        game = play_game(player1, player2)

        if(game.get_winner() == Tile.BLACK):
            black_victories+=1
        elif(game.get_winner() == Tile.WHITE):
            white_victories+=1

    #print(f"Black Wins: {black_victories}")
    #print(f"White Wins: {white_victories}")
    return black_victories, white_victories

"""
Plays a game with the given players and counts the number of legal moves at each turn
"""
def count_legal_moves(player1, player2, print_turns = False):
    #create a new game with the input players
    game = Game(player1, player2)
    
    #initialize a list to store number of legal moves at each turn
    num_legal_moves_per_turn = []
    
    #play through a full game
    while not game.is_over():
        #count legal moves before each turn and record in a list
        num_legal_moves = game.count_legal_moves(game.current_player.color)
        num_legal_moves_per_turn.append(num_legal_moves)
        #take the next turn
        game.take_turn()

    if print_turns == True:
        print("Number of legal moves at each turn")
        for turn, number in enumerate(num_legal_moves_per_turn):
            print(f"Turn {turn}: {number}")
    
    return num_legal_moves_per_turn

"""
This function plays n games, averages the number of legal moves at each turn and uses that to estimate the size of the game tree. 
""" 
def count_game_tree(n, print_turns = False):
    #create two random players
    black_player = RandomPlayer(Tile.BLACK)
    white_player = RandomPlayer(Tile.WHITE)

    #initialize list to store the lists of numbers of legal moves per turn for each game
    legal_moves_per_game = []

    #play n games, counting the number of legal moves at each turn
    for i in range(n):
        print(i)
        num_legal_moves_per_turn = count_legal_moves(black_player, white_player)
        legal_moves_per_game.append(num_legal_moves_per_turn)
    
    # Initialize a list to store the average legal moves at each turn
    turn_averages = []

    # Find the maximum number of turns in a game
    max_turns = max(len(row) for row in legal_moves_per_game)

    # Iterate over each turn
    for turn_index in range(max_turns):
        turn_total = 0
        turn_count = 0

        # Iterate over each game played
        for game in legal_moves_per_game:
            #make sure the turn exists in that game
            if turn_index < len(game):
                #add the number of legal moves to a running total
                turn_total += game[turn_index]
                #increase the count by 1
                turn_count += 1

        # Calculate the average number of legal moves at that turn
        if turn_count > 0:
            turn_average = turn_total / turn_count
            turn_averages.append(turn_average)
        else:
            # If there are no values in a column, you may want to handle it accordingly
            turn_averages.append(None)

    #print the average number of legal moves per turn
    if print_turns == True:
        print("Average number of legal moves per turn")
        for i, average in enumerate(turn_averages):
            print(f"Turn {i}: {average}")

    #estimate game tree size by multiplying legal moves (branches) at each turn
    game_tree_size = 1
    for i, average in enumerate(turn_averages):
        if (i != len(turn_averages)-1):
            game_tree_size *= average

    print(f"Estimated game tree size: {game_tree_size}")

    return game_tree_size

# Set up games to play here
if __name__ == "__main__": 
    #play_game(RandomGreedyPlayer(Tile.BLACK), DefensivePlayer(Tile.WHITE), print_board=True, print_turns=True, print_winner=True, step_through_game = False)
    #play_games(100, RandomGreedyPlayer(Tile.BLACK), BalancedDefensivePlayer(Tile.WHITE))
    play_games(100, BalancedDefensivePlayer(Tile.WHITE), BalancedDefensivePlayer(Tile.BLACK))
    #count_legal_moves(RandomPlayer(Tile.BLACK), RandomPlayer(Tile.WHITE), print_turns=True)
    #count_game_tree(10000)