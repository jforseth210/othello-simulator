from tile import Tile
SYMBOLS = {
    Tile.EMPTY: ' ',
    Tile.WHITE: '○',
    Tile.BLACK: '●'
}
class Game():
    """
    Initialize the game board
    """
    def __init__(self, black_player, white_player):
        self.board = []
        self.game_over = False
        self.white_player = white_player
        self.black_player = black_player
        self.current_player = black_player
        for y in range(8):
            row = []
            for x in range(8):
                square = Tile.EMPTY
                row.append(square)
            self.board.append(row)    
        self.board[3][4] = Tile.WHITE
        self.board[4][3] = Tile.WHITE
        self.board[3][3] = Tile.BLACK
        self.board[4][4] = Tile.BLACK
    
    """
    Ask the current player to make a move, 
    switch to the next player 
    """
    def take_turn(self):
        legal_moves = self.get_legal_moves(self.current_player.color)

        # Neither player has legal moves: game over
        if(legal_moves == [] and self.get_legal_moves(self.get_enemy_player(self.current_player).color)==[]):
            self.game_over = True
            return
        # Passing play to the other player if we have no moves
        elif(legal_moves == []):
            self.current_player = self.get_enemy_player(self.current_player)
            return
        # Ask current player to make a move (make a choice from legal moves)
        move = self.current_player.move(self, legal_moves)
        
        # Implements the player's move
        if move in legal_moves: 
            x,y=move
            self.make_move(x,y)
        
        # Pass play to the other player
        self.current_player = self.get_enemy_player(self.current_player)
        
    """
    Get the opposing player to the given player
    """
    def get_enemy_player(self, player):
        if player == self.black_player: 
            return self.white_player
        elif player == self.white_player: 
            return self.black_player
    """
    Place a tile at the given coords (if legal) and flip
    all flanked tiles 
    """
    def make_move(self, x,y):
        if not self.is_legal(x, y, self.current_player.color):
            return False

        # Place our tile
        self.set_tile(x, y, self.current_player.color)

        # See if there are flanked tiles in any direction from the placed tile
        for dx,dy in self.get_neighbor_relative_coords(x, y):
            # Flip every flanked tile we find
            for flanked_x,flanked_y in self.get_flanked_pieces(x, y, dx, dy, self.current_player.color):
                self.set_tile(flanked_x, flanked_y, self.current_player.color)
        return True
    
    """
    Determine whether a move is legal
    Only moves which "outflank" an opponent's disc are legal
    """
    def is_legal(self,x,y,tile):
        # Test whether space is empty
        if not self.is_empty(x,y):
            return False
        
        for dx,dy in self.get_neighbor_relative_coords(x,y):
            if not self.in_bounds(x+dx,y+dy):
                # Not a square, check the next one
                continue

            # Neighboring tile is an enemy
            if self.get_tile(x+dx,y+dy) == self.get_enemy(tile):
                
                # See if there's actually a flanking
                flanked_pieces = self.get_flanked_pieces(x, y, dx, dy, tile)
                if flanked_pieces:
                    return True

        # No flanking, illegal 
        return False

    """
    Whether or not there's a tile at the given coords
    """
    def is_empty(self,x,y):
        return self.board[y][x] == Tile.EMPTY

    """
    Simple generator to get the relative coordinates of a 
    point's neighbors.
    E.G: Up and right -> (1,-1)
    """
    def get_neighbor_relative_coords(self, x,y): 
        for dx in (-1,0,1):
            for dy in (-1,0,1):
                if dx != 0 or dy != 0:
                    yield dx, dy
    
    """
    Check whether a given coordinate is within the boundries of the board
    """
    def in_bounds(self, x,y):
        return (0 <= x < 8) and (0 <= y < 8)

    """
    Gets the tile at a given point
    """
    def get_tile(self, x,y):
        return self.board[y][x]

    """
    Sets the tile at a given point
    """
    def set_tile(self, x,y, tile):
        self.board[y][x] = tile

    """
    Returns the enemy of the given color
    BLACK -> WHITE
    WHITE -> BLACK
    EMPTY -> EMPTY
    """
    def get_enemy(self, tile):
        if not isinstance(tile, Tile):
            raise ValueError("tile is not a Tile")
        if tile == Tile.BLACK:
            return Tile.WHITE
        elif tile == Tile.WHITE:
            return Tile.BLACK
        else:
            # Garbage in, garbage out
            return Tile.EMPTY
    """
    Returns a list of coordinates where a tile of color
    can legally be placed
    """
    def get_legal_moves(self, color):
        moves = []
        for i in range(8):
            for j in range(8):
                if self.is_legal(i,j, color):
                    moves.append((i,j))
        return moves
    
    """
    Returns the number of legal moves available
    """
    def count_legal_moves(self, color):
        moves = self.get_legal_moves(color)
        count = len(moves)
        return count

    """
    Whether or not the game is over (neither player has legal moves)
    """
    def is_over(self):
        # TODO: Prevent double legal move check
        #return self.get_legal_moves(self.black_player.color) == [] or self.get_legal_moves(self.white_player.color) == []
        return self.game_over
    """
    Determines winner by number of tiles
    """
    def get_winner(self):
        count={
            Tile.WHITE: 0,
            Tile.BLACK: 0,
            Tile.EMPTY: 0
        }
        for row in self.board:
            for value in row:
                count[value]+=1
        if count[Tile.WHITE] > count[Tile.BLACK]:
            return Tile.WHITE
        elif count[Tile.BLACK] > count[Tile.WHITE]:
            return Tile.BLACK
        # Not ideal....
        return Tile.EMPTY
    """
    Returns the coords of tiles that would be flanked by color, checking in the direction indicated by dx/dy
    dx: -1,0,1
    dy: -1,0,1
    """
    def get_flanked_pieces(self, start_x,start_y,dx,dy, color):
        current_x = start_x+dx
        current_y = start_y+dy
        flanked_pieces = []
        while self.in_bounds(current_x, current_y):
            current_tile = self.get_tile(current_x, current_y)
            if current_tile == color:
                # Found one of ours, return enemy pieces 
                # between the starting point and this tile
                return flanked_pieces
            
            elif current_tile == self.get_enemy(color):
                # We found an enemy piece, add to potential flanking
                flanked_pieces.append((current_x, current_y))
                # Keep going
                current_x += dx
                current_y += dy

            elif current_tile == Tile.EMPTY:
                # We found an empty square before one of our pieces, no flanking
                return []
        # We went out of bounds before finding one of our pieces, no flanking
        return []

    """
    Display the board to console
    """
    def print_board(self):
        for row in self.board:
            row_symbols = [SYMBOLS[Tile] for Tile in row]
            print(f"|{'|'.join(row_symbols)}|")

    """
    Completely copy the game state into a new game object. 
    Useful if a player wants to see how a move would affect the game state
    without making a move in the 'real' game. 
    """
    def clone(self):
        clone = Game(self.black_player, self.white_player)
        clone.board =  [row[:] for row in self.board]
        clone.current_player = self.current_player
        return clone