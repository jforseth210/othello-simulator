# Othello Simulator
This program simulates games of Othello with algorithms simulating various player strategies.

Written by Chris Szpilka and Justin Forseth
