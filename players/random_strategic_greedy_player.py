import random
class RandomStrategicGreedyPlayer():
    def __init__(self, color):
        self.color = color

    """
    This function is called every time the game wants the player to make a move
    board is a 2D array of tiles representing the board state
    legal_moves is an array of coordinates where a tile can legally be placed

    Must return a pair of coordinates from legal_moves
    Modifying the board or legal_moves directly is cheating
    """
    def move(self, game, legal_moves):
        # Prefer corners first
        legal_corners=[]
        for corner in [(0,0),(0,7),(7,0),(7,7)]:
            if corner in legal_moves:
                legal_corners.append(corner)
        if legal_corners:
            return random.choice(legal_corners)
    
        # Prefer sides next
        legal_sides = []
        for x,y in legal_moves: 
            if x in [0,7] or y in [0,7]:
                legal_sides.append((x,y))
        if legal_sides:
            return random.choice(legal_sides)

        # Finally prefer move that captures most pieces
        best_move = None
        most_flanked_pieces = 0 
        for x,y in legal_moves:
            flanked_pieces = 0
            # See if there are flanked tiles in any direction from the placed tile
            for dx,dy in game.get_neighbor_relative_coords(x, y):
                # Count every flanked tile we find
                for flanked_x,flanked_y in game.get_flanked_pieces(x, y, dx, dy, self.color):
                    flanked_pieces+=1

            if(flanked_pieces>most_flanked_pieces):
                best_move = (x,y)
                most_flanked_pieces = flanked_pieces
        return best_move