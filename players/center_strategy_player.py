import random
import math
class CenterStrategyPlayer():
    def __init__(self, color):
        self.color = color

    """
    This function is called every time the game wants the player to make a move
    board is a 2D array of tiles representing the board state
    legal_moves is an array of coordinates where a tile can legally be placed

    Must return a pair of coordinates from legal_moves
    Modifying the board or legal_moves directly is cheating
    """
    def move(self, game, legal_moves):
        # Prefer corners first
        legal_corners=[]
        for corner in [(0,0),(0,7),(7,0),(7,7)]:
            if corner in legal_moves:
                legal_corners.append(corner)
        if legal_corners:
            return random.choice(legal_corners)
    
        # Prefer sides next
        legal_sides = []
        for x,y in legal_moves: 
            if x in [0,7] or y in [0,7]:
                legal_sides.append((x,y))
        if legal_sides:
            return random.choice(legal_sides)

        # Finally prefer move in center
        best_move = None
        #Lowest score best 
        best_move_score = 100000
        for x,y in legal_moves:
            if best_move_score>abs(3.5-x)+abs(3.5-y):
                best_move_score = abs(3.5-x)+abs(3.5-y)
                best_move = (x,y)
        return best_move