import time
import random
class DefensivePlayer():
    def __init__(self, color):
        self.color = color

    """
    This function is called every time the game wants the player to make a move
    board is a 2D array of tiles representing the board state
    legal_moves is an array of coordinates where a tile can legally be placed

    Must return a pair of coordinates from legal_moves
    Modifying the board or legal_moves directly is cheating
    """
    def move(self, game, legal_moves):

        #start = time.perf_counter()   
        best_move = None
        best_moves = []
        worst_best_move = 8
        
        for x,y in legal_moves:
            #try making each possible move on a clone game board
            clone = game.clone()
            clone.make_move(x,y)
            #find how many pieces are flanked by my enemy's optimal move on the next turn
            enemy_best_move = self.find_best_enemy_move(clone)
            #choose the move that will give my enemy the lowest-valued optimal move
            #treats the enemy having no moves as having a value of 0: this is the best-case scenario
            if enemy_best_move<worst_best_move:
                best_moves = [(x, y)]
            elif enemy_best_move == worst_best_move:
                best_moves.append((x, y))
        
       
        best_move = random.choice(best_moves)
        
        #print(f"{int((time.perf_counter()-start)*1000)} ms")
        return best_move
    
    def find_best_enemy_move(self, clone):
        #pass play to my enemy
        clone.current_player = clone.get_enemy_player(clone.current_player)
        #find my enemy's legal moves on the next turn
        enemy_moves = clone.get_legal_moves(clone.current_player.color)
        #find the enemy's optimal move following "greedy" protocol
        most_flanked_pieces = 0
        for enemy_x, enemy_y in enemy_moves:
            flanked_pieces = 0
            # See if there are flanked tiles in any direction from the placed tile
            for dx,dy in clone.get_neighbor_relative_coords(enemy_x, enemy_y):
                # Count every flanked tile we find
                for flanked_x,flanked_y in clone.get_flanked_pieces(enemy_x, enemy_y, dx, dy, clone.current_player.color):
                    flanked_pieces+=1

            #find the enemy move with the most flanked pieces
            if(flanked_pieces>most_flanked_pieces):
                most_flanked_pieces = flanked_pieces
        
        return most_flanked_pieces